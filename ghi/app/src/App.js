import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import { useEffect, useState } from 'react';

import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';

import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';

import ModelList from './ModelList';
import ModelForm from './ModelForm';

import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';

import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmenList';
import ServiceHistory from './ServiceHistory';

import SalesList from './SalesList';
import SalesForm from './SalesForm';

import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';

import SalespeopleList from './SalespeopleList';
import SalespersonForm from './SalesPersonForm';
import SalesPersonHistory from './SalesPersonHistory';

function App() {
  const [manufacturers, setManufacturers] = useState([]);

  const getManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const manufacturers = data.manufacturers;
      setManufacturers(manufacturers);
    }
  }


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          {/* Manufacturer Route */}
          <Route path="manufacturers">
            <Route path="list" element={<ManufacturersList  manufacturers={manufacturers} />} />
            <Route path="new" element={<ManufacturerForm manufacturers={manufacturers} getManufacturers={getManufacturers} />} />
          </Route>

          {/* Automobile route */}
          <Route path="automobiles">
            <Route path="list" element={<AutomobileList/>} />
            <Route path="new" element={<AutomobileForm manufacturers={manufacturers} getManufacturers={getManufacturers} />} />
          </Route>

          {/* Model route */}
          <Route path="models">
            <Route path="list" element={<ModelList/>} />
            <Route path="new" element={<ModelForm/>} />
          </Route>

          {/* Technician route */}
          <Route path="technicians">
            <Route path="list" element={<TechnicianList/>} />
            <Route path="new" element={<TechnicianForm/>} />
          </Route>

          {/* Appointment route */}
          <Route path="appointments">
            <Route path="list" element={<AppointmentList/>} />
            <Route path="new" element={<AppointmentForm/>} />
            <Route path="history" element={<ServiceHistory/>} />
          </Route>

          {/* Sales route */}
          <Route path='sales'>
            <Route path='list' element={<SalesList />} />
            <Route path='new' element={<SalesForm />} />
          </Route>

          {/* Customers route */}
          <Route path='customers'>
            <Route path="list" element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>

          {/* Sales Person Route */}
          <Route path='/salespeople'>
            <Route path='list' element={<SalespeopleList />} />
            <Route path='new' element={<SalespersonForm/>} />
            <Route path='history' element={<SalesPersonHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
