import React, { useEffect, useState } from 'react';

function ManufacturersList() {
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8100/api/manufacturers')
      .then(response => response.json())
      .then(data => setManufacturers(data.manufacturers))
      .catch(error => console.error('Error:', error));
  }, []);

  return (
    <>
      <h1 className="mb-3 mt-3">Manufacturers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => (
            <tr key={manufacturer.id}>
              <td>{manufacturer.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default ManufacturersList;
