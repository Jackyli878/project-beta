import React, { useEffect, useState } from 'react';


function TechnicianForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');
    const [state, setState] = useState(false);
    const [duplicate, setDuplicate] = useState(false);


    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value);
    }

    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
    }

    const handleEmployeeIdChange = (event) => {
        setEmployeeId(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;

        const TechnicianUrl = "http://localhost:8080/api/technicians/"
        const fetchConfig = {
            method:"POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(TechnicianUrl, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            setState(true);
        }
        else {
            setState(false);
            setDuplicate(true);
        }
    }

        return(
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Add a Technician</h1>
                <form onSubmit={handleSubmit} id="create-technician-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFirstNameChange} placeholder="firstName" required type="text" name = "firstName" id="firstName" className="form-control" value={firstName}/>
                        <label htmlFor="firstName">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleLastNameChange} placeholder="lastName" required type="text" name = "lastName" id="lastName" className="form-control" value={lastName}/>
                        <label htmlFor="lastName">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeIdChange} placeholder="employeeId" required type="text" name = "employeeId" id="employeeId" className="form-control" value={employeeId}/>
                        <label htmlFor="employeeId">Employee ID</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                <p></p>
                {state?
                    <div className="alert alert-success mb-0" id="success-message">
                    Success! You created a Technician!
                </div>: <div></div>
                }
                <div>
                {duplicate?
                    <div className="alert alert-danger mb-0" id="warning-message">
                    Technician already exists.
                </div>: <div></div>
                }
                </div>
                </div>
            </div>
        </div>
        )


}

export default TechnicianForm;
