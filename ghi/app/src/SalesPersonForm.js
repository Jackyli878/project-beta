import React, { useState } from 'react';

function SalespersonForm({ getSalesperson }) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');
  const [success, setSuccess] = useState(false);

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleEmployeeNumberChange = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeId,
    };

    const salespersonUrl = 'http://localhost:8090/api/salespeople/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salespersonUrl, fetchOptions);
    if (response.ok) {
      setFirstName('');
      setLastName('');
      setEmployeeId('');
      setSuccess(true);
    }
  };

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-salesperson-form">
                <h1 className="card-title">Sign up</h1>
                {success && (
                  <div className="alert alert-success" role="alert">
                    Success! Signed Up as a sales perosn.
                  </div>
                )}
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleFirstNameChange}
                      required
                      placeholder="First Name"
                      type="text"
                      id="first_name"
                      name="first_name"
                      className="form-control"
                      value={firstName}
                    />
                    <label htmlFor="first_name">First Name</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleLastNameChange}
                      required
                      placeholder="Last Name"
                      type="text"
                      id="last_name"
                      name="last_name"
                      className="form-control"
                      value={lastName}
                    />
                    <label htmlFor="last_name">Last Name</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleEmployeeNumberChange}
                      required
                      placeholder="Employee Id"
                      type="text"
                      id="employee_id"
                      name="employee_id"
                      className="form-control"
                      value={employeeId}
                    />
                    <label htmlFor="employee_id">Employee Id</label>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalespersonForm;
