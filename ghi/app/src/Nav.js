import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCentric</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <div className="nav-item dropdown">
              {/* Inventory dropdown */}
              <NavLink className="btn btn-secondary dropdown-toggle bg-success" to="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <li><NavLink className="dropdown-item" to="/manufacturers/list">Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturers/new">Create a new manufacturer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models/list">Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models/new">Create a new model</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles/list">Automobiles</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles/new">Create a new automobile</NavLink></li>
              </ul>

              {/* service dropdown */}
              <NavLink className="btn btn-secondary dropdown-toggle bg-success" to="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                Service
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <li><NavLink className="dropdown-item" to="/technicians/list">Technicians</NavLink></li>
              <li><NavLink className="dropdown-item" to="/technicians/new">Add a Technician</NavLink></li>
              <li><NavLink className="dropdown-item" to="/appointments/list">Appointments</NavLink></li>
              <li><NavLink className="dropdown-item" to="/appointments/new">Create an Appointment</NavLink></li>
              <li><NavLink className="dropdown-item" to="/appointments/history">Service History</NavLink></li>
              </ul>

              {/* Sales dropdown */}
              <NavLink className="btn btn-secondary dropdown-toggle bg-success" to="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <li><NavLink className="dropdown-item" to="/customers/new">New customer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers/list">CustomerList</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/new">New Sales person</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/list">Sales people list</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/history">Sales person history</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/new">New sale</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/list">Sold List</NavLink></li>
              </ul>

            </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}
export default Nav;
