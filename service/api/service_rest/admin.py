from django.contrib import admin
from .models import Technician, AutomobileVO, Appointment


@admin.register(Technician)
class TechnicianConfig(admin.ModelAdmin):
    pass


@admin.register(AutomobileVO)
class AutomobileVOConfig(admin.ModelAdmin):
    pass


@admin.register(Appointment)
class AppointmentConfig(admin.ModelAdmin):
    pass
