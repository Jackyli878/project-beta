# CarCentric

Team:
* Jacky Li - Sales microservice
* Andrew Morales - Service microservice

## Requirements & How to run this app

All of the necessary requirements are listed in the requirements.txt. All of the docker containers needed to run this application have been defined in the docker-compose.yml.
In order to prepare the environments to run this application, the following commands should be ran in the project directory:
    - docker volume create beta-data
    - docker-compose build
    - docker-compose up
    - after a short period of time, when the react server starts up, the application can be accessed through the browser at http://localhost:3000.

Once all of the docker containers are running, you are ready to use CarCentric!

#### Installation/Running the app
1. Fork Repository from https://gitlab.com/Jackyli878/project-beta
2. Change to directory you wish to work in
3. Change to project beta directory
```
cd project-beta
```
4. Clone to your local machine via HTTPS
```
git clone https://gitlab.com/Jackyli878/project-beta
```
5. Input following commands into terminal to build and run docker volume & containers
```
docker volume create beta-data
```
```
docker-compose build
```
```
docker-compose up
```
6. Access app through http://localhost:3000

![Alt text](Readme%20images/CarCentric%20homepage.jpg)

## Design
CarCentric is an application designed for dealerships to handle elements revolving around their Inventory, Sales, and Services.

CarCentric is able to offer seamless interactions between its numerous capabilities by using RESTful APIs, giving dealerships the ability to manage their inventory, sales procedures, and servicing operations effectively. This strategy allows a modular and scalable architecture, allowing for the independent development, updating, and maintenance of each microservice, improving flexibility and simplifying system administration.

### Features

### *Inventory*
1. *Automobiles*
* Create Automobile
* List of Automobiles
2. *Manufacturer*
* Create Manufacturer
* List of Manufacturer
3. *Car Models*
* Create a Car Model
* List Car Models

### *Service*
1. *Technician*
* Create a Technician
* List of Technicians
2. *Services*
* Create Appointments
* List of Appointments
* List of Service History
### *Sales*
1. *Sales*
* Add a Sale
* List of Sales
2. *Sales Person*
* add a Sales Person
* List of Sales People
* List of Sales by each Sales Person
3. *Customers*
* Add a Custoemr
* Customer List

## Diagram
![Alt text](Readme%20images/CarCentric%20Diagram.png)

## API Documentation

### URLs and Ports

### Service microservice API
The Service microservice is hosted on port 8080.

#### Technicians
| Action | HTTP Method | API URL |
| ----------- | ----------- | ----------- |
| List Technicians | GET | http://localhost:8080/api/technicians/
| Technician Details | GET | http://localhost:8080/api/technicians/id
| Create Technician | POST | http://localhost:8080/api/technicians/
| Delete Technician | DELETE | http://localhost:8080/api/technicians/id

**List Technicians:** Send a GET request to the provided url to recieve a list of all technicians at the company.
```
Example Response:
{
	"technicians": [
		{
			"first_name": "Andrew",
			"last_name": "Morales",
			"employee_id": 123456
		},
		{
			"first_name": "John",
			"last_name": "Webb",
			"employee_id": 654321
		}
	]
}
```
**Technician Details:** Send a GET request to the provided url to recieve the details of a single technician.
```
Example Response:
{
	"first_name": "John",
	"last_name": "Webb",
	"employee_id": 654321
}
```
**Create Technician:** Send a POST request with the required JSON body to the provided url to create a new technician in the database.
```
Required JSON body
{
  "first_name": "Andrew",
	"last_name": "Morales",
	"employee_id": "123456"
}
```
```
Example Response:
[
	{
		"first_name": "Andrew",
		"last_name": "Morales",
		"employee_id": "123456"
	},
	true
]
```
**Delete Technician:** Send a DELETE request to the provided url to delete a technician from the database.
```
Example Response:
{
	"deleted": true
}
```

#### Appointments
| Action | HTTP Method | API URL |
| ----------- | ----------- | ----------- |
| List Appointments | GET | http://localhost:8080/api/appointments/
| Appointment Details | GET | http://localhost:8080/api/appointments/id
| Create Appointment | POST | http://localhost:8080/api/appointments/
| Delete Appointment | DELETE | http://localhost:8080/api/appointments/id
| Cancel Appointment | PUT | http://localhost:8080/api/appointments/id/cancel
| Finish Appointment | PUT | http://localhost:8080/api/appointments/id/finish


**List Appointments:** Send a GET request to the provided url to recieve a list of all appointments with the status of "scheduled" for the company. A service history has been provided through the front-end of the application to display a list of all appointments regardless of status.
```
Example Response:
{
	"appointments": [
		{
			"reason": "New Tires",
			"status": "finished",
			"vin": "44445678901232137",
			"customer": "Greg Stevens",
			"id": 5,
			"technician": "Andrew",
			"date_time": "2013-07-06T06:00:00+00:00"
		},
		{
			"reason": "Oil Change",
			"status": "finished",
			"vin": "74932055432768543",
			"customer": "Steve Jones",
			"id": 6,
			"technician": "Andrew",
			"date_time": "2023-06-14T07:15:00+00:00"
		}
    ]
}
```
**Appointment Details:** Send a GET request to the provided url to recieve the details of a single appointment.
```
Example Response:
{
	"reason": "New Tires",
	"status": "scheduled",
	"vin": "44445678901232137",
	"customer": "Greg Stevens",
	"technician": "Andrew",
	"date_time": "2013-07-06T06:00:00+00:00"
}
```
**Create Appointment:** Send a POST request with the required JSON body to the provided url to create a new appointment in the database.
```
Required JSON body
{
	"date_time": "2013-06-08 06:00",
	"vin": "15505678901274367",
	"customer": "Mike Fitzgerald",
	"reason": "windshield",
	"status": "scheduled",
	"technician": "123456"
}
```
```
Example Response:
[
	{
		"reason": "windshield",
		"status": "scheduled",
		"vin": "15505678901274367",
		"customer": "Mike Fitzgerald",
		"id": 14,
		"technician": "Andrew",
		"date_time": "2013-06-08 06:00"
	},
	true
]
```
**Delete Appointment:** Send a DELETE request to the provided url to delete an appointment from the database.
```
Example Response:
{
	"deleted": true
}
```
**Cancel Appointment:** Send a PUT request to the provided url to change the status property of an appointment from "scheduled" to "canceled".
```
Example Response:
{
	"reason": "Oil Change",
	"status": "canceled",
	"vin": "12345678901234567",
	"customer": "John Smith",
	"technician": "Andrew",
	"date_time": "2012-09-04T06:00:00+00:00"
}
```
**Finish Appointment:** Send a PUT request to the provided url to change the status property of an appointment from "scheduled" to "finished".
```
Example Response:
{
	"reason": "Oil Change",
	"status": "finished",
	"vin": "74932055432768543",
	"customer": "Steve Jones",
	"technician": "Andrew",
	"date_time": "2023-06-14T07:15:00+00:00"
}
```
### Inventory microservice API
The Inventory microservice is hosted on port 8100.

#### Vehicle Models
| Action | HTTP Method | API URL |
| ----------- | ----------- | ----------- |
| List Models | GET | http://localhost:8100/api/models/
| Model Details | GET | http://localhost:8100/api/models/
| Create Model | POST | http://localhost:8100/api/models/
| Delete Model| DELETE | http://localhost:8100/api/models/

**List Models:** Send a GET request to the provided url to recieve a list of vehicle models for the company.
```
Example Response:
{
	"models": [
		{
			"href": "/api/models/4/",
			"id": 4,
			"name": "300",
			"picture_url": "https://media.ed.edmunds-media.com/chrysler/300/2021/oem/2021_chrysler_300_sedan_s_fq_oem_1_1280.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrysler"
			}
		}
	]
}
```
**Model Details:** Send a GET request to the provided url to recieve the details of a single vehicle model.
```
Example Response:
{
	"href": "/api/models/4/",
	"id": 4,
	"name": "300",
	"picture_url": "https://media.ed.edmunds-media.com/chrysler/300/2021/oem/2021_chrysler_300_sedan_s_fq_oem_1_1280.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}
```
**Create Model** Send a POST request with the required JSON body to the provided url to create a new vehicle model in the database.
```
Required JSON body:
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```
```
Example Response:
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}
```
**Delete Model** Send a DELETE request to the provided url to delete a vehicle model from the database.
```
Example response:
{
	"id": null,
	"name": "300",
	"picture_url": "https://media.ed.edmunds-media.com/chrysler/300/2021/oem/2021_chrysler_300_sedan_s_fq_oem_1_1280.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}
```

#### Manufacturers
| Action | HTTP Method | API URL |
| ----------- | ----------- | ----------- |
| List Manufacturers | GET | http://localhost:8100/api/manufacturers
| Manufacturer Details | GET | http://localhost:8100/api/manufacturers/id
| Create Manufacturer | POST | http://localhost:8100/api/manufacturers/
| Delete Manufacturer| DELETE | http://localhost:8100/api/manufactures/id

**List Manufacturers:** Send a GET request to the provided url to recieve a list of manufacturers for the company.
```
Example Response:
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	]
}
```
**Manufacturer Details:** Send a GET request to the provided url to recieve the details of a single manufacturer.
```
Example Response:
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Chrysler"
}
```
**Create Manufacturer:** Send a POST request with the required JSON body to the provided url to create a new manufacturer in the database.
```
Required JSON body:
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Chrysler"
}
```
```
Example Response:
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "chevrolet"
}
```
**Delete Manufacturer:** Send a DELETE request to the provided url to delete a manufacturer from the database.
```
Example Response:
{
	"id": null,
	"name": "chevrolet"
}
```

#### Automobiles
| Action | HTTP Method | API URL |
| ----------- | ----------- | ----------- |
| List Automobiles | GET | http://localhost:8100/api/automobiles
| Automobile Details | GET | http://localhost:8100/api/automobiles/vin
| Create Automobile | POST | http://localhost:8100/api/automobiles/
| Delete Automobile| DELETE | http://localhost:8100/api/automobiles/vin

**List Automobiles:** Send a GET request to the provided url to recieve a list of automobiles for the company.
```
Example Response:
{
	"autos": [
		{
			"href": "/api/automobiles/38828591045672867/",
			"id": 2,
			"color": "Red",
			"year": 2020,
			"vin": "38828591045672867",
			"model": {
				"href": "/api/models/5/",
				"id": 5,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chrysler"
				}
			},
			"sold": false
		}
	]
}
```
**Automobile Details:** Send a GET request to the provided url to recieve the details of a single automobile.
```
Example Response:
{
	"autos": [
		{
			"href": "/api/automobiles/38828591045672867/",
			"id": 2,
			"color": "Red",
			"year": 2020,
			"vin": "38828591045672867",
			"model": {
				"href": "/api/models/5/",
				"id": 5,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chrysler"
				}
			},
			"sold": false
		}
	]
}
```
**Create Automobile:** Send a POST request with the required JSON body to the provided url to create a new automobile in the database.
```
Required JSON body:
{
			"color": "black",
			"model_id": "5",
			"vin": "23478943298765654",
			"year": "2021"

		}

```
```
Example Response:
{
	"href": "/api/automobiles/23478943298765654/",
	"id": 5,
	"color": "black",
	"year": "2021",
	"vin": "23478943298765654",
	"model": {
		"href": "/api/models/5/",
		"id": 5,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```
**Delete Automobile:** Send a DELETE request to the provided url to delete an automobile from the database.
```
Example Response:
{
	"href": "/api/automobiles/38828591045672867/",
	"id": null,
	"color": "Red",
	"year": 2020,
	"vin": "38828591045672867",
	"model": {
		"href": "/api/models/5/",
		"id": 5,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```

### Sales Microservice
The sales microservice is hosted at port 8090.

#### Customers
| Action | HTTP Method | API URL |
| ----------- | ----------- | ----------- |
| List Customers | GET | http://localhost:8090/api/customers
| Create Customer | POST | http://localhost:8090/api/customers
| Delete Customer | DELETE | http://localhost:8090/api/customers/id


**List Customers:**: Send a GET request to the provided url to recieve a list of customers.
```
Example Response:
{
	"customers": [
		{
			"first_name": "John",
			"last_name": "James",
			"address": "1234 Very Real Drive, City, State, Zip Code",
			"phone_number": "555-555-5555",
			"id": 1
		}
	]
}
```
**Create Customer:** Send a POST request with the required JSON body to the provided url to create a new customer in the database.
```
Required JSON body:
{
	"address": "513 Very Real Drive, City, State, Zip Code",
	"first_name": "Jimmy",
	"last_name": "John",
	"phone_number": "555-555-5555"
}
```
```
Example Response:
{
	"first_name": "Jimmy",
	"last_name": "John",
	"address": "513 Very Real Drive, City, State, Zip Code",
	"phone_number": "555-555-5555",
	"id": 2
}
```
**Delete Customer:** Send a DELETE request to the provided url to delete a customer from the database.
```
Example Response:
{
	"deleted": true
}
```

#### Salesperson
| Action | HTTP Method | API URL |
| ----------- | ----------- | ----------- |
| List Salespeople | GET | http://localhost:8090/api/salespeople
| Create Salesperson | POST | http://localhost:8090/api/salespeople
| Delete Salesperson | DELETE | http://localhost:8090/api/salespeople/id

**List Salespeople:**: Send a GET request to the provided url to recieve a list of salespeople.
```
Example Response:
{
	"sales_employee": [
		{
			"first_name": "Lucy",
			"last_name": "Jones",
			"employee_id": "444321",
			"id": 1
		}
	]
}
```
**Create Salesperson:** Send a POST request with the required JSON body to the provided url to create a new customer in the database.
```
Required JSON body:
{
	"first_name": "Greg",
	"last_name": "Carman",
	"employee_id": "123456"
}
```
```
Example Response:
{
	"first_name": "Greg",
	"last_name": "Carman",
	"employee_id": "123456",
	"id": 2
}
```
**Delete Salesperson:** Send a DELETE request to the provided url to delete a salesperson from the database.
```
Example Response:
{
	"deleted": true
}
```

#### Sales
| Action | HTTP Method | API URL |
| ----------- | ----------- | ----------- |
| List Sales | GET | http://localhost:8090/api/sales
| Create Sales | POST | http://localhost:8090/api/sales
| Delete Sale | DELETE | http://localhost:8090/api/sales/id

**List Sales:** Send a GET request to the provided url to recieve a list of all sales made for the company.
```
Example Response:
{
	"sales": []
}
```
**Create Sale:** Send a POST request with the required JSON body to the provided url to create a new sale in the database.
```
Required JSON body:
{
	"salesperson": "13" ,
	"customer": "8",
	"automobile": "1JCMR7841JT185472",
	"price": "48000"
}
```
```
Example Response:
{
	"automobile": {
		"vin": "1JCMR7841JT185472"
	},
	"salesperson": {
		"first_name": "Bobert",
		"last_name": "Robert",
		"employee_id": "13",
		"id": 11
	},
	"customer": {
		"first_name": "Jan",
		"last_name": "Lee",
		"address": "895 Address street, Las Vegas NV",
		"phone_number": "(909)-574-9635",
		"id": 8
	},
	"price": "48000",
	"id": 10
}
```
**Delete Sale:** Send a DELETE request to the provided url to delete a salesperson from the database.
```
Example Response:
{
	"deleted": true
}
```
### Inventory microservice
The Inventory microservice manages the Inventory of automobiles within CarCentric. The microservice has implemented models that contains the specific details of an autombile such as the make (manufacturer), model, vin, color, and year of a car. This information is necessary for the functionality of both the sales and microservice.

### *Inventory Models*
1. Manufacturer
- (manufacturer) name
2. VehicleModel
- (Vehicle Model) name
- picture url
- manufacturer (one to many relationship foreign key)
3. Automobile
- color
- year
- vin
- sold
- model (one to many relationship foreign key)

### Service microservice

Explain your models and integration with the inventory
microservice, here.

#### Models:
I created the models for the service microservice in order to provide functionality for creating service Appointments, adding Technicians to be assigned to those appointments, and polling data from the inventory api to store that data in an automobile value object.
    - Technician Model: includes the first name, last name, and employee id for each technician.
    - Appointment Model: includes the "datetime" of the appointment, the reason for the appointment, the status of the appointment, the VIN of the vehicle to be serviced, the name of the customer that scheduled the appointment, and the technician assigned to the appointment.
    -AutomobileVO Model: includes the VIN and sold status of the automobile entities in the inventory.

#### Integration:
The poller.py in the service microservice polls the inventory api every 60 seconds to get automobile data and update or create an AutomobileVO.
This is important because it allows the microservice to use that data when creating and tracking service appointments.

### Sales microservice
The sales microservice was created to handle the one to many relationship between AutomobileVO and native models in Sales. The models in sales is as follows

### *Sales Models*
1. AutmobileVO
- vin
- sold
2. SalesPerson
- first name
- last name
- employee_id
3. Customer
- first name
- last name
- address
- phone_number
4. Sales
- automobile
- salesperson
- customer

The AutomobileVO model primarly provides the vin to many of the list and create features related to sales. It also has an attribute "sold" that helps manage a list of avaliable vehicles for creating a new sale. The create a new sale feature has foreign keys tied to both SalesPerson and Customer, which helps to bring up a list of existing Sales people and Customers to select.

## Value Objects
AutomobileVO is the primary value object in this application. It is used to draw property data from the the automobile model database in the Inventory microservice and store it in the AutomobileVO model for use within the Sales and Services microservice. This is done by the poller which polls information from and endpoint, and stores the information polled into the Value Objet model.
